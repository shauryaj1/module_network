package com.example.module_network.model

import com.google.gson.annotations.SerializedName

data class AccountInfo(val email: String)

data class Authorization(@SerializedName("user_id") val userId: String,
                            @SerializedName("token") val token: String)