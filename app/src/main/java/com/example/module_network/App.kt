package com.example.module_network

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.example.module_network.interceptors.AuthorizationInterceptor
import com.example.module_network.interceptors.NetworkConnectionInterceptor
import com.example.module_network.interceptors.TokenRenewInterceptor
import com.example.module_network.listeners.AuthenticationListener
import com.example.module_network.listeners.InternetConnectionListener
import com.example.module_network.listeners.Session
import com.example.module_network.utils.SP_KEY_EMAIL
import com.example.module_network.utils.SP_KEY_PASSWORD
import com.example.module_network.utils.SP_KEY_TOKEN
import com.example.module_network.utils.SharedPrefUtil
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class App : Application(){

    private var session: Session? = null
    private var apiService: ApiService? = null
    var authenticationListener: AuthenticationListener? = null
    var mInternetConnectionListener: InternetConnectionListener? = null

    public fun getApiService(): ApiService {
        if (apiService == null) {
            apiService = provideRetrofit(ApiService.URL).create(ApiService::class.java)
        }
        return apiService!!
    }

    private fun isInternetAvailable(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    private fun provideRetrofit(url: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(url)
            .client(provideOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()
    }

    private fun provideOkHttpClient(): OkHttpClient {
        val okhttpClientBuilder = OkHttpClient.Builder()
        okhttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS)
        okhttpClientBuilder.readTimeout(30, TimeUnit.SECONDS)
        okhttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS)

        okhttpClientBuilder.addInterceptor(object : NetworkConnectionInterceptor(){
            override fun onInternetUnavailable() {
                mInternetConnectionListener?.onInternetUnavailable()
            }

            override val isInternetAvailable: Boolean
                get() = isInternetAvailable()

        })

        okhttpClientBuilder.addInterceptor(
            TokenRenewInterceptor(
                getSession()
            )
        )

        okhttpClientBuilder.addInterceptor(
            AuthorizationInterceptor(
                getApiService(),
                getSession()
            )
        )
        return okhttpClientBuilder.build()
    }

    private fun getSession(): Session {
        if (session == null) {
            session = object : Session {
                override fun saveEmail(email: String) = SharedPrefUtil.getInstance(this@App).saveString(
                    SP_KEY_EMAIL, email)


                override fun savePassword(password: String) = SharedPrefUtil.getInstance(this@App).saveString(
                    SP_KEY_PASSWORD, password)

                override fun saveToken(token: String) = SharedPrefUtil.getInstance(this@App).saveString(
                    SP_KEY_TOKEN, token)

                override val isLoggedIn: Boolean = SharedPrefUtil.getInstance(this@App).getString(
                    SP_KEY_TOKEN
                ).isNotEmpty()

                override val token: String = SharedPrefUtil.getInstance(this@App).getString(
                    SP_KEY_TOKEN
                )

                override val email: String = SharedPrefUtil.getInstance(this@App).getString(
                    SP_KEY_EMAIL
                )

                override val password: String = SharedPrefUtil.getInstance(this@App).getString(
                    SP_KEY_PASSWORD
                )

                override fun invalidate() {
                    if (authenticationListener != null) {
                        authenticationListener!!.onUserLoggedOut()
                    }
                }
            }
        }

        return session!!
    }

}