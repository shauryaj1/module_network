package com.example.module_network

import com.example.module_network.model.AccountInfo
import com.example.module_network.model.Authorization
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.POST




interface ApiService {

    companion object {
        const val URL = "https://mindtickle.com"
    }

    @GET("accounts/{accountId}")
    fun getAccountInfo(@Path("accountId") accountId: String): Call<AccountInfo>

    @POST("login")
    fun loginAccount(@Header("Authorization") authKey: String): Call<Authorization>

}