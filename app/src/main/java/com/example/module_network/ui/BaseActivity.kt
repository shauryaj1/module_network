package com.example.module_network.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.module_network.App
import com.example.module_network.listeners.AuthenticationListener
import com.example.module_network.listeners.InternetConnectionListener
import com.example.module_network.model.AccountInfo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class BaseActivity : AppCompatActivity(), InternetConnectionListener, AuthenticationListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val app = application as App
        app.authenticationListener = this
        app.mInternetConnectionListener = this

        validateUser(app)
    }


    //TODO  api calling through repository using observables
    private fun validateUser(app: App) {
        app.getApiService().getAccountInfo("")
            .enqueue(object : Callback<AccountInfo>{
                override fun onFailure(call: Call<AccountInfo>, t: Throwable) {
                    // fire AuthActivityIntent
                }

                override fun onResponse(call: Call<AccountInfo>, response: Response<AccountInfo>) {

                }
            })

    }





    override fun onUserLoggedOut() {
        // fire AuthActivityIntent
    }


    override fun onInternetUnavailable() {
        // fire Dialog UI
    }

}