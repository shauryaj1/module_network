package com.example.module_network.listeners

interface InternetConnectionListener {
    fun onInternetUnavailable()
}