package com.example.module_network.listeners

interface AuthenticationListener {
    fun onUserLoggedOut()
}