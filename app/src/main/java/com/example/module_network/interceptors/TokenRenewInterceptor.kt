package com.example.module_network.interceptors

import com.example.module_network.listeners.Session
import okhttp3.Interceptor
import okhttp3.Response

class TokenRenewInterceptor(private val session: Session) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

        val newToken = response.header("x-auth-token")
        if (newToken != null) {
            session.saveToken(newToken!!)
        }

        return response
    }
}