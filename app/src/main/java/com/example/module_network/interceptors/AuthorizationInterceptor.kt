package com.example.module_network.interceptors

import android.util.Base64
import com.example.module_network.ApiService
import com.example.module_network.listeners.Session
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response



class AuthorizationInterceptor(private val apiService: ApiService, private val session: Session) : Interceptor {

    override fun intercept(chain: Chain): Response {
        var mainResponse = chain.proceed(chain.request())
        val mainRequest = chain.request()

        if (session.isLoggedIn) {
            if (mainResponse.code() == 401 || mainResponse.code() == 403) {
                val authKey = getAuthorizationHeader(session.email, session.password)
                // request to login API to get fresh token synchronously calling login API
                val loginResponse = apiService.loginAccount(authKey).execute()

                if (loginResponse.isSuccessful) {
                    // login request succeed, new token generated
                    val authorization = loginResponse.body()
                    session.saveToken(authorization!!.token)
                    val builder = mainRequest.newBuilder().header("Authorization", session.token)
                        .method(mainRequest.method(), mainRequest.body())
                    mainResponse = chain.proceed(builder.build())
                }
            }
        }

        return mainResponse
    }

    private fun getAuthorizationHeader(email: String, password: String): String {
        val credential = "$email:$password"
        return "Basic " + Base64.encodeToString(credential.toByteArray(), Base64.DEFAULT)
    }
}