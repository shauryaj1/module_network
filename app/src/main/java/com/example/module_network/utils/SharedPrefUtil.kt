package com.example.module_network.utils

import android.app.Application
import android.content.Context
import android.content.SharedPreferences


class SharedPrefUtil(val context: Application){

    private var sharedPref: SharedPreferences? = null

    companion object{

        @Volatile private var INSTANCE: SharedPrefUtil? = null

        fun getInstance(context: Application): SharedPrefUtil {
            if (INSTANCE == null){
                INSTANCE =
                    SharedPrefUtil(context)
            }
            return INSTANCE!!
        }
    }

    init {
        sharedPref = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
    }

    fun saveString(key: String, value: String){
        val editor = sharedPref!!.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getString(key: String): String = sharedPref!!.getString(key, "")!!

}