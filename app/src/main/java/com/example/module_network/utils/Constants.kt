package com.example.module_network.utils

const val SP_NAME = "pref"
const val SP_KEY_EMAIL = "email"
const val SP_KEY_PASSWORD = "pass"
const val SP_KEY_TOKEN = "token"